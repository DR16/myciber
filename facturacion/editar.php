<?php
if(!isset($_GET["idProducto"])) exit();
$id = $_GET["idProducto"];
include_once "conexion.php";
$sentencia = $conn->prepare("SELECT * FROM productos WHERE idProducto = ?;");
$sentencia->execute([$id]);
$producto = $sentencia->fetch(PDO::FETCH_OBJ);
if($producto === FALSE){
	echo "¡No existe algún producto con ese id!";
	exit();
}

?>
<?php include_once "encabezado3.php" ?>
	<div class="col-xs-11"><br><br><br>
		<h1>Editar producto con el id <?php echo $producto->idProducto; ?></h1>
		<form method="post" action="guardarDatosEditados.php">
			<input type="hidden" name="idProducto" value="<?php echo $producto->idProducto; ?>">
	
			<label for="codigoBarras">Código de barras:</label>
			<input value="<?php echo $producto->codigoBarras ?>" class="form-control" name="codigoBarras" required type="text" id="codigoBarras" placeholder="Escribe el código">

			<label for="nombreProducto">Nombre del Producto:</label>
			<input value="<?php echo $producto->nombreProducto ?>" class="form-control" name="nombreProducto" required type="text" id="nombreProducto" placeholder="Escribe el código">

			<label for="medidaProducto">Medida del Producto:</label>
			<input value="<?php echo $producto->medidaProducto ?>" class="form-control" name="medidaProducto" required type="text" id="medidaProducto" placeholder="Medida del Producto">

			<label for="categoriaProducto">Categoria del Producto:</label>
			<input value="<?php echo $producto->categoriaProducto ?>" class="form-control" name="categoriaProducto" required type="text" id="categoriaProducto" placeholder="Categoria del Porducto">

			<label for="costoProducto">Costo del Producto:</label>
			<input value="<?php echo $producto->costoProducto ?>" class="form-control" name="costoProducto" required type="decimal" id="costoProducto" placeholder="Costo del Producto">

			<label for="ivaProducto">iva del Producto:</label>
			<input value="<?php echo $producto->ivaProducto ?>" class="form-control" name="ivaProducto" required type="decimal" id="ivaProducto" placeholder="iva del Producto">

			<label for="PrecioProducto">Precio del Producto:</label>
			<input value="<?php echo $producto->PrecioProducto ?>" class="form-control" name="PrecioProducto" required type="decimal" id="PrecioProducto" placeholder="Precio del Producto">

			<label for="proveedorProducto">Proveedor del Producto:</label>
			<input value="<?php echo $producto->proveedorProducto ?>" class="form-control" name="proveedorProducto" required type="text" id="proveedorProducto" placeholder="Proveedor del Producto">

			<label for="Existencia">Existencia del Producto:</label>
			<input value="<?php echo $producto->Existencia ?>" class="form-control" name="Existencia" required type="text" id="Existencia" placeholder="Existencia del Producto">

			<label for="idproveedor">id del Proveedor:</label>
			<input value="<?php echo $producto->idproveedor ?>" class="form-control" name="idproveedor" required type="text" id="idproveedor" placeholder="id del Proveedor">

			<br><br><input class="btn btn-default" type="submit" value="Guardar">
			<a class="btn btn-danger" href="./producto.php">Cancelar</a>
		</form>
	</div>
<?php include_once "pie.php" ?>
