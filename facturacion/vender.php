<?php 
include_once "encabezado3.php";
session_start();
if(!isset($_SESSION["carrito"])) $_SESSION["carrito"] = [];

?>
	<div class="col-xs-12">
		<h1>Vender</h1>
		<?php
			if(isset($_GET["status"])){
				if($_GET["status"] === "1"){
					?>
						<div class="alert alert-success">
							<strong>¡Correcto!</strong> Venta realizada correctamente
						</div>
					<?php
				}else if($_GET["status"] === "2"){
					?>
					<div class="alert alert-info">
							<strong>Venta cancelada</strong>
						</div>
					<?php
				}else if($_GET["status"] === "3"){
					?>
					<div class="alert alert-info">
							<strong>Ok</strong> Producto quitado de la lista
						</div>
					<?php
				}else if($_GET["status"] === "4"){
					?>
					<div class="alert alert-warning">
							<strong>Error:</strong> El producto que buscas no existe
						</div>
					<?php
				}else if($_GET["status"] === "5"){
					?>
					<div class="alert alert-danger">
							<strong>Error: </strong>El producto está agotado
						</div>
					<?php
				}else{
					?>
					<div class="alert alert-danger">
							<strong>Error:</strong> Algo salió mal mientras se realizaba la venta
						</div>
					<?php
				}
			}
		?>
		<br>
		<form method="post" action="agregarAlCarrito.php">
			<label for="codigoBarras">Código de barras:</label>
			<input autocomplete="off" autofocus class="form-control" name="codigoBarras" required type="text" id="codigoBarras" placeholder="Escribe el código">
		</form>
		<br><br>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>idVenta</th>
					<th>Codigo</th>
					<th>Nombre</th>
					<th>Precio</th>
					<th>formaPago</th>
					<th>SumaGravada</th>
					<th>IVA</th>
					<th>totalVentas</th>
					<th>fecha Registro</th>
					<th>idusuario</th>
					<th>Quitar</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($_SESSION["carrito"] as $indice => $venta){ 
						
					?>
				<tr>
					<td><?php echo $venta->idVenta ?></td>
					<td><?php echo $venta->codigoBarras?></td>
					<td><?php echo $venta->PrecioProducto ?></td>
					<td><?php echo $venta->idcliente ?></td>
					<td><?php echo $venta->formaPago ?></td>
					<td><?php echo $venta->SumaGravada ?></td>
					<td><?php echo $venta->iva ?></td>
					<td><?php echo $venta->totalVentas ?></td>
					<td><?php echo $venta->fechaRegistroSistema ?></td>
					<td><?php echo $venta->idusuario ?></td>
					<td><a class="btn btn-danger" href="<?php echo "quitarDelCarrito.php?indice=" . $indice?>"><i class="fa fa-trash"></i></a></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>

		
		<form action="./terminarVenta.php" method="POST">
			<input name="total" type="hidden" value="<?php echo $granTotal;?>">
			<button type="submit" class="btn btn-default">Terminar venta</button>
			<a href="./cancelarVenta.php" class="btn btn-danger">Cancelar venta</a>
		</form>
	</div>
<?php include_once "pie.php" ?>