<?php
if(!isset($_GET["idProducto"])) exit();
$idProducto = $_GET["idProducto"];
include_once "./conexion.php";
$sentencia = $conn->prepare("DELETE FROM productos WHERE idProducto = ?;");
$resultado = $sentencia->execute([$idProducto]);
if($resultado === TRUE){
	header("Location: ./producto.php");
	exit;
}
else echo "Algo salió mal";
?>