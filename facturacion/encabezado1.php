<?php
session_start();

if(isset($_SESSION['idUSuario']) == null || isset($_SESSION['idUSuario']) == ''){
	header('Location: ../index.php');
}else if(isset($_SESSION['idUSuario'])){
	
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Facturacion</title>
	
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<style>
  	.navbar-nav > li > a, .navbar-brand {
    padding-top:15px !important; padding-bottom:0 !important;
    height: 52px; /* aqui escoges el alto*/
    }
    .dropdown-menu>li > a,.dropdown-menu {
    background-color: #000000;
    color:#ffffff;
  }
  .btn-default {
      box-shadow: 1px 2px 5px #82AB82;
      background: #13DA16;
	  color: #fff;   
     }
     .btn-danger {
      box-shadow: 1px 2px 5px #82AB82;
      background: #FF0101;
	  color: #fff;   
     }
     .btn-info {
      box-shadow: 1px 2px 5px #82AB82;
      background: #1BD7FC;
	  color: #fff;   
     }
     table thead {
	  color: #000;
	  background-color: #FF9E00;
	   border: black 2.5px solid;
	 }
	 table tbody {
	  border: black 2.5px solid;
	}
  </style>
</head>
<body>
	
<nav class="navbar navbar-inverse navbar-fixed-top">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" style="color:#fff">
	     <font size=4.5><span class="glyphicon glyphicon-home"></span>My Ciber</font>
	 	 </a>
	    </div>
	    <ul class="nav navbar-nav" >
	      <li class="dropdown">
	        <a class="dropdown-toggle" style="color:#fff" data-toggle="dropdown" href="#"><font size=4.5>Modulos
	        <span class="caret"></span></a></font>
	        <ul class="dropdown-menu">
	          <li><a href="">Inventario</a></li>
	          <li class="divider"></li>
	          <li><a href="../Proveedores/Proveedores.php">Proveedor</a></li>
	          <li class="divider"></li>
	          <li><a href="../prueba.php">Home</a></li>
	          <li class="divider"></li>
	        </ul>
	      </li>
	    </ul>
	     <ul class="nav navbar-nav">
			<li><a style="color:#fff"href="./producto.php"><font size=4.5>Productos</font></a></li>
			<li><a style="color:#fff"href=""><font size=4.5>vender</font></a></li>
			<li><a style="color:#fff"href=""><font size=4.5>ventas</font></a></li>
		</ul>	
	    <ul class="nav navbar-nav navbar-right">
	      	<li><a href="../cerrar_session.php"><font size=4.5><span class="glyphicon glyphicon-log-out"></span>Sign off</font></a></li>
	   	 </ul>
	  </div>
	</nav>

	 <div class="navbar navbar-inverse navbar-fixed-bottom">
    <div class="container">
      <p class="navbar-text pull-left">&copy <?php echo date('Y');?> - Programacion PHP.
      </p>
   </div>