<?php include_once "encabezado3.php" ?>
<?php
include_once "conexion.php";
$sentencia = $conn->query("SELECT * FROM ventas;");
$ventas = $sentencia->fetchAll(PDO::FETCH_OBJ);
?>

	<div class="col-xs-11"><br><br><br>
		<h1>Ventas</h1>
		<div>
			<a class="btn btn-default" href="./vender.php">Nueva <i class="glyphicon glyphicon-plus"></i></a>
		</div>
		<br>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>idVenta</th>
					<th>numero de Factura</th>
					<th>fecha de Factura</th>
					<th>idcliente</th>
					<th>forma de Pago</th>
					<th>Suma Gravada</th>
					<th>iva</th>
					<th>totalVentas</th>
					<th>fechaRegistroSistema</th>
					<th>idusuario</th>
					<th>Eliminar</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($ventas as $venta){ ?>
				<tr>
					<td><?php echo $venta->idVenta ?></td>
					<td><?php echo $venta->numFacturaVenta?></td>
					<td><?php echo $venta->fechaFactura ?></td>
					<td><?php echo $venta->idcliente ?></td>
					<td><?php echo $venta->formaPago ?></td>
					<td><?php echo $venta->SumaGravada ?></td>
					<td><?php echo $venta->iva ?></td>
					<td><?php echo $venta->totalVentas ?></td>
					<td><?php echo $venta->fechaRegistroSistema ?></td>
					<td><?php echo $venta->idusuario ?></td>
					
					<td><a class="btn btn-danger" href="<?php echo "eliminarVenta.php?idVenta=" . $venta->idVenta?>"><i class="glyphicon glyphicon-trash"></i></a></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
<?php include_once "pie.php" ?>