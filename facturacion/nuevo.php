<?php
#Salir si alguno de los datos no está presente
if(!isset($_POST["idProducto"]) || !isset($_POST["codigoBarras"]) || !isset($_POST["nombreProducto"]) || !isset($_POST["medidaProducto"]) || !isset($_POST["categoriaProducto"]) || !isset($_POST["costoProducto"]) || !isset($_POST["ivaProducto"]) || !isset($_POST["PrecioProducto"]) || !isset($_POST["proveedorProducto"]) || !isset($_POST["Existencia"]) || !isset($_POST["idproveedor"])) exit();


#Si todo va bien, se ejecuta esta parte del código...

include_once "./conexion.php";
$idProducto = $_POST["idProducto"];
$codigoBarras = $_POST["codigoBarras"];
$nombreProducto = $_POST["nombreProducto"];
$medidaProducto = $_POST["medidaProducto"];
$categoriaProducto = $_POST["categoriaProducto"];
$costoProducto = $_POST["costoProducto"];
$ivaProducto = $_POST["ivaProducto"];
$PrecioProducto = $_POST["PrecioProducto"];
$proveedorProducto = $_POST["proveedorProducto"];
$Existencia = $_POST["Existencia"];
$idproveedor = $_POST["idproveedor"];


$sentencia = $conn->prepare("INSERT INTO productos(idProducto, codigoBarras, nombreProducto, medidaProducto, categoriaProducto, costoProducto, ivaProducto, PrecioProducto, proveedorProducto, Existencia, idproveedor) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
$resultado = $sentencia->execute([$idProducto, $codigoBarras, $nombreProducto, $medidaProducto, $categoriaProducto, $costoProducto, $ivaProducto, $PrecioProducto, $proveedorProducto, $Existencia, $idproveedor]);

if($resultado === TRUE){
	header("Location: ./producto.php");
	exit;
}
else echo "Algo salió mal. Por favor verifica que la tabla exista";


?>