<?php
$usuario ="root";
$nombreDB = "myciber";
$contrasena = "";

try {
	$conn = new PDO("mysql:host=localhost;dbname=".$nombreDB, $usuario, $contrasena);
	$conn->query("set names utf8;");
    $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
	
}
catch(PDOException $e) {
	echo "Ocurrió algo con la base de datos: " . $e->getMessage();
}
?>