<?php include_once "./encabezado1.php" ?>
<?php
include_once "../configuraciones/conexion.php";

$sentencia = $conn->query("SELECT * FROM productos;");
$productos = $sentencia->fetchAll(PDO::FETCH_OBJ);
?>

	<div class="col-xs-11"><br><br><br>
		<h1>Productos</h1>
		<div>
			<a class="btn btn-default" href="./formProducto.php">Nuevo <i class="glyphicon glyphicon-plus"></i></a>
		</div>
		<br>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>idProducto</th>
					<th>Código</th>
					<th>Nombre</th>
					<th>Medida</th>
					<th>Categoria</th>
					<th>Costo</th>
					<th>IVA</th>
					<th>Precio</th>
					<th>Proveedor</th>
					<th>Existencia</th>
					<th>idProveedor</th>
					<th>Editar</th>
					<th>Eliminar</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($productos as $producto){ ?>
				<tr>
					<td><?php echo $producto->idProducto ?></td>
					<td><?php echo $producto->codigoBarras ?></td>
					<td><?php echo $producto->nombreProducto ?></td>
					<td><?php echo $producto->medidaProducto ?></td>
					<td><?php echo $producto->categoriaProducto ?></td>
					<td><?php echo $producto->costoProducto ?></td>
					<td><?php echo $producto->ivaProducto ?></td>
					<td><?php echo $producto->PrecioProducto ?></td>
					<td><?php echo $producto->proveedorProducto ?></td>
					<td><?php echo $producto->Existencia ?></td>
					<td><?php echo $producto->idproveedor ?></td>
					
					<td>
						<a href="<?php echo "editar.php?idProducto=" . $producto->idProducto?>" class="btn btn-info btn-lg"> <span class="glyphicon glyphicon-pencil" style="color:white"></span></a>
					</td>
					<td>
					  <a href="<?php echo "eliminar.php?idProducto=" . $producto->idProducto?>" class="btn btn-danger btn-lg"><span class="glyphicon glyphicon-trash"style="color:white"></span> </a>
					</td>

				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
<?php include_once "pie.php" ?>