<?php
session_start();


if(isset($_SESSION['idUSuario']) == null || isset($_SESSION['idUSuario']) == ''){

}else if(isset($_SESSION['idUSuario'])){
	header('Location: ciber/prueba.php');
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="ciber/librerias/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="ciber/librerias/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="ciber/librerias/css/util.css">
	<link rel="stylesheet" type="text/css" href="ciber/librerias/css/main.css">
<!--===============================================================================================-->
</head>
<body>

	<div class="limiter">
		<div class="container-login100" style="background-image: url('ciber/librerias/images/bg-01.jpg');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-41">
					Inicio de Sesion
				</span>
				<form class="login100-form validate-form p-b-33 p-t-5" id="form_login" action="">

					<div class="wrap-input100 validate-input" data-validate = "Debe de ingresar un usuario">
						<input class="input100" type="text" name="txt_usuario" placeholder="Ingrese Usuario">
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Debe de ingresar una contraseña">
						<input class="input100" type="password" id="pass" name="txt_password" placeholder="Ingrese Contraseña">
						<span class="focus-input100" data-placeholder="&#xe80f;"></span>
					</div>

					<div class="container-login100-form-btn m-t-32">
                        <button type="buuton" class="login100-form-btn" name="" onclick="logueo();">Ingresar</button>
						
					</div>

				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>

<!--======================================================================-->
	<script src="ciber/librerias/vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="ciber/librerias/vendor/animsition/js/animsition.min.js"></script>
	<script src="ciber/librerias/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="ciber/librerias/js/main.js"></script>
    <script src="ciber/login/controlador.js"></script>

</body>
</html>
