jQuery(document).on('submit', '#form_login', function(event) {
    event.preventDefault();

    jQuery.ajax({
        url: 'ciber/login/login.php',
        type: 'POST',
        dataType: 'json',
        data: $(this).serialize(),
        beforeSend: function() {
            $('.login100-form-btn').val('Validando..');
        }
    })

    //Obtenemos la respuesta del servidor
    .done(function(respuesta) {

        console.log(respuesta);
        if (!respuesta.error) {
            location.href = 'ciber/prueba.php';
        } else {
            $('#pass').val('');
            alert("Error al ingresar sus datos");
            $('.login100-form-btn').val('Login');
        }
    })

    .fail(function(resp) {
        console.log(resp.responseText);
    })

    .always(function() {
        console.log("complete");
    });
});