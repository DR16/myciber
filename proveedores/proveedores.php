<?php include_once "encabezado1.php" ?>
<?php
include_once "../configuraciones/conexion.php";

$sentencia = $conn->query("SELECT * FROM proveedores;");
$productos = $sentencia->fetchAll(PDO::FETCH_OBJ);
?>

	<div class="col-xs-11"><br><br><br>
		<h1>Proveedor</h1>
		<div>
			<a class="btn btn-default" href="formProveedor.php">Nuevo <i class="glyphicon glyphicon-plus"></i></a>
		</div>
		<br>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>NombreProveedor</th>
					<th>Telefono</th>
					<th>Direccion</th>
					<th>email</th>
					<th>Editar</th>
					<th>Eliminar</th>		
									
				</tr>
			</thead>
			<tbody>
				<?php foreach($proveedor as $proveedor){ ?>
				<tr>
					<td><?php echo $proveedor->idProveedor ?></td>
					<td><?php echo $proveedor->nombreProveedor ?></td>
					<td><?php echo $proveedor->telefono ?></td>
					<td><?php echo $proveedor->direccion ?></td>
					<td><?php echo $proveedor->email ?></td>
					
					<td>
						<a href="<?php echo "editar.php?idProveedor=" . $proveedor->idProveedor?>" class="btn btn-info btn-lg"> <span class="glyphicon glyphicon-pencil" style="color:white"></span></a>
					</td>
					<td>
					  <a href="<?php echo "eliminar.php?idProveedor=" . $proveedor->idProveedor?>" class="btn btn-danger btn-lg"><span class="glyphicon glyphicon-trash"style="color:white"></span> </a>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
<?php include_once "../Facturacion/pie.php" ?>