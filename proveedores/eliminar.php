<?php
if(!isset($_GET["idProveedor"])) exit();
$idProveedor = $_GET["idProveedor"];
include_once "../configuraciones/conexion.php";
$sentencia = $conn->prepare("DELETE FROM proveedor WHERE idProveedor = ?;");
$resultado = $sentencia->execute([$idProveedor]);
if($resultado === TRUE){
	header("Location: ./proveedores.php");
	exit;
}
else echo "Algo salió mal";
?>