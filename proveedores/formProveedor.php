<?php include_once "./encabezado1.php" ?>

<div class="col-xs-11"><br><br><br>
	<h1>Nuevo proveedor</h1>
	<form method="post" action="nuevo.php">
		<label for="idProveedor">Codigo del Proveedor:</label>
		<input class="form-control" name="idProveedor" required type="number" id="idProveedor" placeholder="Escribe el código">

		<label for="nombreProveedor">Nombre del Proveedor:</label>
		<input class="form-control" name="nombreProveedor" required type="text" id="nombreProveedor" placeholder="Nombre del Proveedor">

		<label for="telefono">Telefono del Proveedor:</label>
		<input class="form-control" name="telefono" required type="text" id="telefono" placeholder="Telefono del Proveedor">

		<label for="direccion">Direccion del Proveedor:</label>
		<input class="form-control" name="direccion" required type="text" id="direccion" placeholder="Direccion del Proveedor">

		<label for="email">email del Proveedor:</label>
		<input class="form-control" name="email" required type="text" id="email" placeholder="email del Proveedor">
		<br><br><input class="btn btn-info" type="submit" value="Guardar">
	</form>
</div>

<?php include_once "./pie.php" ?>