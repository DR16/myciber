<?php
#Salir si alguno de los datos no está presente
if(!isset($_POST["idProveedor"]) || !isset($_POST["nombreProveedor"]) || !isset($_POST["telefono"]) || !isset($_POST["direccion"]) || !isset($_POST["email"])) exit();

#Si todo va bien, se ejecuta esta parte del código...

include_once "../configuraciones/conexion.php";
$idProveedor = $_POST["idProveedor"];
$nombreProveedor = $_POST["nombreProveedor"];
$telefono = $_POST["telefono"];
$direccion = $_POST["direccion"];
$email = $_POST["email"];

$sentencia = $conn->prepare("INSERT INTO proveedor(idProveedor, nombreProveedor, telefono, direccion, email) VALUES (?, ?, ?, ?, ?);");
$resultado = $sentencia->execute([$idProveedor, $nombreProveedor, $telefono, $direccion, $email]);

if($resultado === TRUE){
	header("Location: ./proveedores.php");
	exit;
}
else echo "Algo salió mal. Por favor verifica que la tabla exista";


?>